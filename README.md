
# LCSB LaTeX beamer template

This is a simple work-alike of the official LCSB slide templates, heavily
inspired by the Metropolis theme.

Clone this and type `make`, if your TeX distribution is okay, the example
`slides.tex` should get compiled to `slides.pdf`.

Proper documentation will be written once this gets used a bit and all rough
edges are cleaned up; in the meantime it is likely better to just follow the
example layouts in `slides.tex`.

Open issues/send merge requests in case of any ideas.
