
slides.pdf: $(wildcard *.tex) $(wildcard *.sty) $(wildcard media/*)
	lualatex slides
	lualatex slides

clean:
	echo "Cleaning latex is almost impossible to do right; use `git clean`."
